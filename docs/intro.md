# PygPeg 

**pygpeg** (/pig'peg/) is a parsing expression grammar (PEG) library for
Python. It allows you to generate a parser that allows you to parse
context-free grammars into whatever structures you want.

**pygpeg** combines the PEG syntax with the terse callback semantics found in
traditional parser generators like yacc or bison. 
