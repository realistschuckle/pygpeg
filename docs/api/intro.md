# The API

The PygPeg library has four modules. You can get to each
from here.

## Exceptions

The [`pygpeg.exceptions`](./exceptions) module provides the
exceptions that your code may encounter as it parses a
grammar.

## Helpers

The [`pygpeg.helpers`](./helpers) module provides some
easy-to-use functions that can be used to indicate different
types of white space.

## Parser

The [`pygpeg.parser`](./parser) module provides classes to
use to build a parser with callbacks.

## Symbols

The [`pygpeg.symbols`](./symbols) module provides the
different discrete classes used to build a parser.
