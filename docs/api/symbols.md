# `pygpeg.symbols`

```{eval-rst}
.. autoclass:: pygpeg.symbols.Symbol
        :members:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Choice
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Empty
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Literal
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Matchp
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Notp
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.OneOrMore
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Optional
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Regex
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.Sequence
        :show-inheritance:
```

---

```{eval-rst}
.. autoclass:: pygpeg.symbols.ZeroOrMore
        :show-inheritance:
```
