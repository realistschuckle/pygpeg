from pygpeg.parser import Rule
from pygpeg.symbols import Literal as lit, Symbol
import pytest
from unittest.mock import MagicMock
from .utils import MockSymbol


def test_name_argument_must_be_string():
    with pytest.raises(TypeError):
        Rule(MagicMock(), None, Symbol())


def test_symbol_argument_must_be_symbol_or_str():
    with pytest.raises(TypeError):
        Rule(MagicMock(), "name", None)
    Rule(MagicMock(), "name", MockSymbol())
    Rule(MagicMock(), "name", "symbol")


def test_callback_argument_must_be_callable():
    with pytest.raises(TypeError):
        Rule(None, "name", Symbol())


def test_rule_name():
    rule = Rule(MagicMock(), "name", MockSymbol())
    assert rule.name == "name"


def test_rule_and_callback_on_success_with_symbol():
    callback = MagicMock(return_value=119)
    symbol = MockSymbol(return_value=(True, 9, ["119"]))
    rule = Rule(callback, "name", symbol)
    matches, index, value = rule.match("Some content", 0)
    assert matches
    assert index == 9
    assert value == 119
    symbol.assert_called_once_with("Some content", 0)
    callback.assert_called_once_with(["119"])


def test_rule_and_callback_on_success_with_name():
    callback = MagicMock(return_value=119)
    symbol = MockSymbol(return_value=(True, 9, ["119"]))
    registry = {"symbol": symbol}
    rule = Rule(callback, "name", "symbol")
    rule.registry = registry
    matches, index, value = rule.match("Some content", 0)
    assert matches
    assert index == 9
    assert value == 119
    symbol.assert_called_once_with("Some content", 0)
    callback.assert_called_once_with(["119"])


def test_fule_and_callback_on_failure():
    callback = MagicMock(return_value=119)
    symbol = MockSymbol(return_value=(False, -1, None))
    rule = Rule(callback, "name", symbol)
    matches, index, value = rule.match("Some content", 0)
    assert not matches
    assert index == 0
    assert value is None
    symbol.assert_called_once_with("Some content", 0)
    callback.assert_not_called()


def test_registry_is_set_on_symbol():
    callback = MagicMock()
    symbol = lit("abc")
    rule = Rule(callback, "my_rule", symbol)
    rule.registry = {}
    assert symbol.registry == rule.registry


def test_str():
    callback = MagicMock()
    symbol = MockSymbol()
    symbol.__str__.return_value = "<mock>"
    rule = Rule(callback, "my_rule", symbol)
    assert str(rule) == "my_rule <- <mock>"

    rule = Rule(callback, "my_rule", "symbol")
    assert str(rule) == "my_rule <- symbol"
