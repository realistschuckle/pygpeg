from pygpeg.symbols import Choice as choice
from unittest.mock import MagicMock
from .utils import MockSymbol


def create_and_match(content, first, second):
    symbol = choice(first, second)
    return symbol.match(content, 1)


def test_sequence_succeeds_if_first_option_matches():
    content = "just some content"
    first = MockSymbol(return_value=(True, 6, ["just"]))
    second = MockSymbol(return_value=(True, 8, ["some"]))
    matches, index, value = create_and_match(content, first, second)
    assert matches
    assert index == 6
    assert value == ["just"]
    first.assert_called_once_with(content, 1)
    second.assert_not_called()


def test_sequence_succeeds_if_first_fails_and_second_succeeds():
    content = "just some content"
    first = MockSymbol(return_value=(False, -1, None))
    second = MockSymbol(return_value=(True, 8, ["some"]))
    matches, index, value = create_and_match(content, first, second)
    assert matches
    assert index == 8
    assert value == ["some"]
    first.assert_called_once_with(content, 1)
    second.assert_called_once_with(content, 1)


def test_sequence_fails_if_both_fail():
    content = "just some content"
    first = MockSymbol(return_value=(False, -1, None))
    second = MockSymbol(return_value=(False, -1, None))
    matches, index, value = create_and_match(content, first, second)
    assert not matches
    assert index == 1
    assert value is None
    first.assert_called_once_with(content, 1)
    second.assert_called_once_with(content, 1)


def test_registry_lookup():
    content = "just some content"
    first = MockSymbol(return_value=(False, -1, None))
    second = MockSymbol(return_value=(False, -1, None))
    registry = {
        "first": first,
        "second": second,
    }
    symbol = choice("first", "second")
    symbol.registry = registry
    matches, index, value = symbol.match(content, 1)
    assert not matches
    assert index == 1
    assert value is None
    first.assert_called_once_with(content, 1)
    second.assert_called_once_with(content, 1)


def test_str():
    first = MagicMock()
    first.__str__.return_value = "<first>"
    second = MagicMock()
    second.__str__.return_value = "<second>"
    assert str(choice(first, second)) == "(<first> / <second>)"
    assert str(choice("first", second)) == "(first / <second>)"
    assert str(choice(first, "second")) == "(<first> / second)"
    assert str(choice("first", "second")) == "(first / second)"
