from pygpeg.symbols import ZeroOrMore as zed
import pytest
from unittest.mock import call, MagicMock
from .utils import MockSymbol


@pytest.mark.parametrize("i", range(5))
def test_zero_or_more_calls_symbols_until_failed_match(i):
    content = "just some content" * i
    return_values = []
    calls = []
    values = []
    for j in range(i):
        calls.append(call(content, j * 4))
        return_values.append((True, (j + 1) * 4, j))
        values.append(j)
    return_values.append((False, -1, None))
    pattern = MockSymbol(side_effect=return_values)
    symbol = zed(pattern)
    matches, index, value = symbol.match(content, 0)
    assert matches
    assert index == i * 4
    assert value == values
    pattern.assert_has_calls(calls)


def test_registry_lookup():
    content = "Hello, world, this is me, Curtis."
    pattern = MockSymbol(return_value=(False, -1, None))
    registry = {"pattern": pattern}
    symbol = zed("pattern")
    symbol.registry = registry
    symbol.match(content, 10)
    pattern.assert_called_once_with(content, 10)


def test_str():
    pattern = MagicMock()
    pattern.__str__.return_value = "<pattern>"
    assert str(zed(pattern)) == "(<pattern>)*"
    assert str(zed("named")) == "(named)*"
