from pygpeg.symbols import Literal as lit, Regex as re, Sequence as seq
import pytest
from unittest.mock import MagicMock
from .utils import MockSymbol


@pytest.mark.parametrize("i", [1, 2, 3])
def test_sequence_calls_symbols_until_failed_match(i):
    content = "just some content"
    mocks = []
    for j in range(i):
        mocks.append(MockSymbol(return_value=(True, (j + 1) * 2 + 1, [j])))
    mocks.append(MockSymbol(return_value=(False, -1, None)))
    symbol = seq(*mocks)
    matches, index, value = symbol.match(content, 1)
    assert not matches
    assert index == 1
    assert value is None
    for j in range(i + 1):
        mocks[j].assert_called_once_with(content, j * 2 + 1)


@pytest.mark.parametrize("i", [1, 2, 3])
def test_sequence_calls_callback_when_all_symbols_match(i):
    content = "just some content"
    patterns = []
    for j in range(i + 1):
        patterns.append(MockSymbol(return_value=(True, (j + 1) * 2 + 1, j)))
    symbol = seq(*patterns)
    matches, index, value = symbol.match(content, 1)
    assert matches
    assert index == (i + 1) * 2 + 1
    assert value == list(range(i + 1))
    for j in range(i + 1):
        patterns[j].assert_called_once_with(content, j * 2 + 1)


def test_sequence_of_literals():
    content = "my age is 123"
    ws = re("\\s")
    my = lit("my")
    age = lit("age")
    is_ = lit("is")
    num = re("\\d+")
    symbol = seq(my, ws, age, ws, is_, ws, num)
    matches, length, value = symbol.match(content, 0)
    assert matches
    assert length == len(content)
    assert value == ["my", " ", "age", " ", "is", " ", "123"]


def test_registry_lookup():
    content = "my age is 123"
    ws = re("\\s")
    my = lit("my")
    age = lit("age")
    is_ = lit("is")
    num = re("\\d+")
    registry = {"ws": ws, "my": my, "age": age, "is": is_, "num": num}
    symbol = seq("my", "ws", "age", "ws", "is", "ws", "num")
    symbol.registry = registry
    matches, length, value = symbol.match(content, 0)
    assert matches
    assert length == len(content)
    assert value == ["my", " ", "age", " ", "is", " ", "123"]


def test_str():
    first = MagicMock()
    first.__str__.return_value = "<first>"
    second = "second"
    third = MagicMock()
    third.__str__.return_value = "<third>"
    assert str(seq(first, second, third)) == "(<first> second <third>)"
