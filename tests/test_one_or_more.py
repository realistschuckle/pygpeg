from pygpeg.symbols import OneOrMore as one
import pytest
from unittest.mock import call, MagicMock
from .utils import MockSymbol


def test_one_or_more_fails_on_no_match():
    content = "just some content"
    pattern = MockSymbol(return_value=(False, -1, None))
    symbol = one(pattern)
    matches, index, value = symbol.match(content, 7)
    assert not matches
    assert index == 7
    assert value is None


@pytest.mark.parametrize("i", range(1, 5))
def test_one_or_more_calls_symbols_until_failed_match(i):
    content = "just some content" * i
    return_values = []
    calls = []
    values = []
    for j in range(i):
        calls.append(call(content, j * 4))
        return_values.append((True, (j + 1) * 4, j))
        values.append(j)
    return_values.append((False, -1, [i]))
    pattern = MockSymbol(side_effect=return_values)
    symbol = one(pattern)
    matches, index, value = symbol.match(content, 0)
    assert matches
    assert index == i * 4
    assert value == values
    pattern.assert_has_calls(calls)


def test_registry_lookup():
    content = "Hello, world, this is me, Curtis."
    pattern = MockSymbol(side_effect=[(True, 2, [12]), (False, -1, None)])
    registry = {"pattern": pattern}
    symbol = one("pattern")
    symbol.registry = registry
    symbol.match(content, 10)
    pattern.assert_has_calls([call(content, 10), call(content, 2)])


def test_str():
    pattern = MagicMock()
    pattern.__str__.return_value = "<pattern>"
    assert str(one(pattern)) == "(<pattern>)+"
    assert str(one("named")) == "(named)+"
