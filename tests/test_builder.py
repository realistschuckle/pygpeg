import operator
from pygpeg.helpers import optional_space as osp
from pygpeg.parser import (
    StringParser,
    ParserBuilder as Builder,
    IncompleteParseError,
    UnknownCallbackError,
)
import pytest
from unittest.mock import ANY, call, MagicMock, patch, PropertyMock


@pytest.mark.parametrize("grammar", ["", None])
def test_builder_fails_on_empty_string_and_none(grammar):
    builder = Builder()
    with pytest.raises(ValueError):
        builder.build(grammar)


def test_builder_handles_math_expression_grammar():
    def math(values):
        """
        Potential values of parameter:
        * Expr rule is an int
        * Sum rule is     [int, [['+-', int], ['+-', int], ...]
        * Product rule is [int, [['*/', int], ['*/', int], ...]
        * Power rule is   [int, None] or [int, ['^', int]]
        * Value rule is   str or [str, int, str]
        """
        if isinstance(values, str) or isinstance(values, int):
            return int(values)
        if len(values) == 2 and (values[1] is None or values[1] == []):
            return values[0]
        elif len(values) == 3 and values[0] == "(" and values[2] == ")":
            return values[1]
        left = values[0]
        if values[1][0] == "^":
            op = operator.pow
            right = values[1][1]
            return op(left, right)
        else:
            for opc, right in values[1]:
                if opc == "-":
                    op = operator.sub
                elif opc == "*":
                    op = operator.mul
                elif opc == "/":
                    op = operator.floordiv
                elif opc == "+":
                    op = operator.add
                left = op(left, right)
            return left

    grammar = """
        Expr    ← Sum                               <math>
        Sum     ← Product (('+' / '-') Product)*    <math>
        Product ← Power (('*' / '/') Power)*        <math>
        Power   ← Value ('^' Power)?                <math>
        Value   ← |[0-9]+| / '(' Expr ')'           <math>
    """
    parser = Builder({"math": math}).build(grammar)
    assert parser.parse("1*3*4^2+9+12/4-2*(1^1+3^(2-1))") == 52


def ignore(values):
    pass


def test_builder_handles_if_statements():
    grammar = """
        S ← 'if ' C 'then ' S 'else ' S / 'if ' C 'then ' S / C   <ignore>
        C ← 'whatever' osp                                        <ignore>
    """
    callback_registry = {"ignore": ignore}
    parser = Builder(callback_registry=callback_registry).build(grammar)
    parser.add_rule("osp", ignore, osp())
    with pytest.raises(IncompleteParseError):
        parser.parse("if whatever then")
    parser.parse(
        "if whatever then if whatever then whatever "
        "else if whatever then whatever"
    )


def test_builder_handles_nested_pascal_comments():
    grammar = """
        C     ← Begin N* End            <ignore>
        Begin ← '(*'                    <ignore>
        End   ← '*)'                    <ignore>
        N     ← C / (!Begin !End Z)     <ignore>
        Z     ← |.|                     <ignore>
    """
    callback_registry = {"ignore": ignore}
    parser = Builder(callback_registry=callback_registry).build(grammar)
    with pytest.raises(IncompleteParseError):
        parser.parse("(* hello (* this is *) incomplete")
    parser.parse("(* hello (* this is *) complete *)")


def test_builder_handles_non_context_free_an_bn_cn():
    grammar = """
        S ← &(A 'c') 'a'+ B !|.|    <ignore>
        A ← 'a' A? 'b'              <count>
        B ← 'b' B? 'c'              <count>
    """
    count = MagicMock()
    callback_registry = {"ignore": ignore, "count": count}
    parser = Builder(callback_registry=callback_registry).build(grammar)
    with pytest.raises(IncompleteParseError):
        parser.parse("aaabbbcc")
    count.reset_mock()
    parser.parse("aaaaabbbbbccccc")
    assert count.call_count == 10


def test_builder_raises_unknown_callback_when_name_not_in_registry():
    grammar = """
        S <- "Hello"  <ignore>
    """
    parser = Builder()
    with pytest.raises(UnknownCallbackError) as info:
        parser.build(grammar)
    assert info.value.name == "ignore"


def test_builder_configures_provided_parser():
    grammar = """
        A <- B <a>
        B <- C <b>
        C <- D <c>
        D <- "done" <d>
    """
    rule_names = ["A", "B", "C", "D"]
    callback_registry = {"a": ignore, "b": ignore, "c": ignore, "d": ignore}
    patch_name = "pygpeg.parser.StringParser.registry"
    with patch(patch_name, new_callable=PropertyMock) as m:
        parser = StringParser()
        parser.add_rule = add_rule = MagicMock()
        builder = Builder(callback_registry=callback_registry)
        builder.build(grammar, parser=parser)
        assert len(add_rule.call_args_list) == 4
        for call_args, name in zip(add_rule.call_args_list, rule_names):
            assert call_args[0][0].name == name
