from pygpeg.symbols import Notp as notp
from unittest.mock import MagicMock
from .utils import MockSymbol


def test_notp_matches_on_value_and_returns_original_index():
    content = "This is a really great piece of content."
    pattern = MockSymbol(return_value=(True, 18, ["boop"]))
    symbol = notp(pattern)
    matches, index, value = symbol.match(content, 4)
    assert not matches
    assert index == 4
    assert value is None
    pattern.assert_called_once_with(content, 4)


def test_notp_fails_on_value_and_returns_original_index():
    content = "How many eggs can you put in a frog?"
    pattern = MockSymbol(return_value=(False, -1, None))
    symbol = notp(pattern)
    matches, index, value = symbol.match(content, 4)
    assert matches
    assert index == 4
    assert value is True
    pattern.assert_called_once_with(content, 4)


def test_registry_lookup():
    content = "How many eggs can you put in a frog?"
    pattern = MockSymbol(return_value=(False, -1, None))
    symbol = notp("pattern")
    symbol.registry = {"pattern": pattern}
    matches, index, value = symbol.match(content, 4)
    assert matches
    assert index == 4
    assert value is True
    pattern.assert_called_once_with(content, 4)


def test_str():
    pattern = MagicMock()
    pattern.__str__.return_value = "<pattern>"
    assert str(notp(pattern)) == "!(<pattern>)"
    assert str(notp("pattern")) == "!(pattern)"
