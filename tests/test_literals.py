from pygpeg.symbols import Empty as empty, Literal as lit, Regex as re
import pytest


@pytest.mark.parametrize("i", range(49))
def test_empty_literal(i):
    content = "this is just some content that will always match."
    symbol = empty()
    matches, length, value = symbol.match(content, i)
    assert matches
    assert length == 0
    assert value == ""


exact_literal_data = [
    ("literal string", "literal", 0, True),
    ("a literal string", "iter", 3, True),
    ("does not match", "not", 0, False),
    ("still does not match", "match", 8, False),
    ("over", "overachiever", 0, False),
]


@pytest.mark.parametrize("content,literal,index,expected", exact_literal_data)
def test_exact_literal(content, literal, index, expected):
    symbol = lit(literal)
    matches, length, value = symbol.match(content, index)
    assert matches == expected
    if matches:
        assert value == literal
        assert length == len(literal) + index
    else:
        assert length == -1
        assert value is None


re_literal_data = [
    ("does not match", "nope*", 0, False, None, None),
    ("you me me you us", "(you|me)( (you|me))*", 0, True, 13, "you me me you"),
    ("still does not match", "ill", 8, False, None, None),
    ("inside 123 345", r"\d+", 7, True, 10, "123"),
    ("over", "overachiever", 0, False, None, None),
]
re_literal_data_params = "content,literal,index,expected,outdex,group"


@pytest.mark.parametrize(re_literal_data_params, re_literal_data)
def test_re_literal(content, literal, index, expected, outdex, group):
    symbol = re(literal)
    matches, length, value = symbol.match(content, index)
    assert matches == expected
    if matches:
        assert value == group
        assert length == outdex
    else:
        assert length == -1
        assert value is None


def test_string_representation():
    assert str(re("[ab]*")) == "|[ab]*|"
    assert str(lit("thingy")) == '"thingy"'
    assert str(empty()) == '""'
