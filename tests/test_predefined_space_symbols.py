from pygpeg.helpers import optional_space, required_space


def test_optional_space_matches_on_space():
    matches, index, value = optional_space().match("   a", 0)
    assert matches
    assert index == 3
    assert value == "   "


def test_optional_space_matches_on_no_space():
    matches, index, value = optional_space().match("abcde", 0)
    assert matches
    assert index == 0
    assert value == ""


def test_required_space_matches_on_space():
    matches, index, value = required_space().match("   a", 0)
    assert matches
    assert index == 3
    assert value == "   "


def test_required_space_fails_on_no_space():
    matches, index, value = required_space().match("abcde", 0)
    assert not matches
    assert index == -1
    assert value is None
