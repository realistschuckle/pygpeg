from pygpeg.parser import MethodRegistryParser, NoGrammarError
import pytest
from unittest.mock import MagicMock


def test_raises_no_grammar_error_when_no_grammar_is_supplied():
    class NoGrammarParser(MethodRegistryParser):
        pass

    with pytest.raises(NoGrammarError):
        NoGrammarParser()


def test_builds_self_from_grammar_attribute():
    class HasGrammarAttribute(MethodRegistryParser):
        grammar = """
            A <- "yes"  <method>
        """
        method = MagicMock(return_value="100%")

    parser = HasGrammarAttribute()
    assert parser.parse("yes") == "100%"


def test_grammar_parameter_takes_precedence_over_attribute():
    class NoGrammarParser(MethodRegistryParser):
        def method(self, values):
            return "no"

    parser = NoGrammarParser("A<-'yes' <method>")
    assert parser.parse("yes") == "no"


def test_math_grammar_with_method_registry_parser():
    class MathExpressionInterpreter(MethodRegistryParser):
        grammar = """
            Expr    ← Sum                               <expr>
            Sum     ← Product (('+' / '-') Product)*    <sum>
            Product ← Power (('*' / '/') Power)*        <product>
            Power   ← Value ('^' Power)?                <power>
            Value   ← |[0-9]+| / '(' Expr ')'           <value>
        """

        def expr(self, values):
            return values

        def sum(self, values):
            sum = values[0]
            for op, value in values[1]:
                if op == "+":
                    sum += value
                elif op == "-":
                    sum -= value
            return sum

        def product(self, values):
            product = values[0]
            for op, value in values[1]:
                if op == "*":
                    product *= value
                elif op == "/":
                    product //= value
            return product

        def power(self, values):
            power = values[0]
            if values[1] is not None:
                power = power ** values[1][1]
            return power

        def value(self, values):
            if isinstance(values, str):
                return int(values)
            return int(values[1])

    parser = MathExpressionInterpreter()
    assert parser.parse("1+2*3/(4-5)") == -5
