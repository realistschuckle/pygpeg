from pygpeg.parser import (
    IncompleteParseError,
    StringParser,
    NoRulesError,
    Rule,
    UnboundSymbolError,
)
from pygpeg.symbols import Literal as lit, Sequence as seq
import pytest
from unittest.mock import MagicMock
from .utils import MockSymbol


def test_parser_has_registry():
    parser = StringParser()
    assert len(parser.registry) == 0


def test_adding_a_rule_adds_rule_name_to_parser():
    rule_name = "my_rule"
    parser = StringParser()
    parser.add_rule(rule_name, lambda: None, MockSymbol())
    assert rule_name in parser.registry


def test_parse_calls_match_on_first_rule():
    content = "hello, world."
    firstcb = MagicMock(return_value="hello")
    firstsym = MockSymbol(return_value=(True, len(content), "hello"))
    secondcb = MagicMock()
    secondsym = MockSymbol()
    parser = StringParser()
    parser.add_rule("first", firstcb, firstsym)
    parser.add_rule("second", secondcb, secondsym)
    parser.parse(content)
    firstsym.assert_called_once_with(content, 0)
    firstcb.assert_called_once_with("hello")
    secondsym.assert_not_called()
    secondcb.assert_not_called()


def test_parse_fails_with_no_rules():
    parser = StringParser()
    with pytest.raises(NoRulesError):
        parser.parse("hello, world.")


def test_parse_returns_match_value_if_all_content_consumed():
    content = "Hello, world"
    sym = MockSymbol(return_value=(True, len(content), ["a", "b"]))
    cb = MagicMock(return_value=18)
    parser = StringParser()
    parser.add_rule("my_rule", cb, sym)
    result = parser.parse(content)
    assert result == 18


def test_parse_raises_incomplete_parse_if_not_all_consumed():
    content = "Hello, world"
    sym = MockSymbol(return_value=(True, len(content) - 1, ["a", "b"]))
    cb = MagicMock(return_value=18)
    parser = StringParser()
    parser.add_rule("my_rule", cb, sym)
    with pytest.raises(IncompleteParseError) as info:
        parser.parse(content)
    assert info.value.index == len(content) - 1
    assert info.value.value == 18


def test_parse_ignores_incomplete_parse_when_instructed():
    content = "Hello, world"
    sym = MockSymbol(return_value=(True, len(content), ["a", "b"]))
    cb = MagicMock(return_value=18)
    parser = StringParser()
    parser.add_rule("my_rule", cb, sym)
    result = parser.parse(content, ignore_incomplete_parse=True)
    assert result == 18


def test_parse_raises_unbound_name_when_using_grammar_with_unbound_name():
    rules = [
        Rule(MagicMock(), "first", seq("second", "third")),
        Rule(MagicMock(), "second", lit("whatever")),
        Rule(MagicMock(), "third", "missing"),
    ]
    parser = StringParser()
    for rule in rules:
        parser.add_rule(rule, None, None)
    with pytest.raises(UnboundSymbolError) as info:
        parser.parse("skidoosh!")
    assert info.value.unbound_name == "missing"
    assert info.value.rule.name == "third"
