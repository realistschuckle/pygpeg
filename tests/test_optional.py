from pygpeg.symbols import Optional as opt
from unittest.mock import MagicMock
from .utils import MockSymbol


def test_optional_succeeds_on_not_match():
    content = "Here is some text, kids."
    pattern = MockSymbol(return_value=(False, -1, None))
    symbol = opt(pattern)
    matches, index, value = symbol.match(content, 1)
    assert matches
    assert index == 1
    assert value is None
    pattern.assert_called_once_with(content, 1)


def test_options_succeeds_on_match():
    content = "Here is some more text, kids."
    pattern = MockSymbol(return_value=(True, 8, "hello"))
    symbol = opt(pattern)
    matches, index, value = symbol.match(content, 1)
    assert matches
    assert index == 8
    assert value == "hello"
    pattern.assert_called_once_with(content, 1)


def test_registry_lookup():
    content = "Well, what about eggs? Who doesn't like eggs?"
    pattern = MockSymbol(return_value=(False, -1, None))
    registry = {"pattern": pattern}
    symbol = opt("pattern")
    symbol.registry = registry
    matches, index, value = symbol.match(content, 10)
    pattern.assert_called_once_with(content, 10)


def test_str():
    pattern = MagicMock()
    pattern.__str__.return_value = "<pattern>"
    assert str(opt(pattern)) == "(<pattern>)?"
    assert str(opt("pattern")) == "(pattern)?"
