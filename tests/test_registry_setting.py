from pygpeg import symbols


def test_choice_registry():
    registry = {}
    first = symbols.Literal("abc")
    second = symbols.Literal("bca")
    choice = symbols.Choice(first, second)
    choice.registry = registry
    assert first.registry == registry
    assert second.registry == registry


def test_matchp_registry():
    registry = {}
    symbol = symbols.Literal("abc")
    matchp = symbols.Matchp(symbol)
    matchp.registry = registry
    assert symbol.registry == registry


def test_notp_registry():
    registry = {}
    symbol = symbols.Literal("abc")
    notp = symbols.Notp(symbol)
    notp.registry = registry
    assert symbol.registry == registry


def test_one_or_more_regisrty():
    registry = {}
    symbol = symbols.Literal("abc")
    one = symbols.OneOrMore(symbol)
    one.registry = registry
    assert symbol.registry == registry


def test_optional_registry():
    registry = {}
    symbol = symbols.Literal("abc")
    opt = symbols.Optional(symbol)
    opt.registry = registry
    assert symbol.registry == registry


def test_sequence_registry():
    registry = {}
    symbol = symbols.Literal("abc")
    seq = symbols.Sequence(symbol)
    seq.registry = registry
    assert symbol.registry == registry


def test_zero_or_more_registry():
    registry = {}
    symbol = symbols.Literal("abc")
    zero = symbols.ZeroOrMore(symbol)
    zero.registry = registry
    assert symbol.registry == registry
