from pygpeg.parser import Rule
from pygpeg.symbols import Match, Symbol
from unittest.mock import MagicMock, PropertyMock


class MatchTrackingMixin:
    def __init__(self, *args, **kwargs):
        d = {
            k: v
            for k, v in kwargs.items()
            if k == "side_effect" or k == "return_value"
        }
        self.match = MagicMock(**d)

    def assert_called_once_with(self, *args):
        self.match.assert_called_once_with(*args)

    def assert_has_calls(self, *args, **kwargs):
        self.match.assert_has_calls(*args, **kwargs)

    def assert_not_called(self):
        self.match.assert_not_called()

    @property
    def __str__(self):
        return self.match.__str__


class MockRule(MatchTrackingMixin, Rule):
    name = None

    def __init__(self, name, *args, **kwargs):
        if "return_value" not in kwargs and "side_effect" not in kwargs:
            kwargs["return_value"] = (False, -1, None)
        MatchTrackingMixin.__init__(self, *args, **kwargs)
        self.name_mock = PropertyMock(return_value=name)

    @property
    def name(self):
        return self.name_mock.__get__(self.name_mock, self)

    @property
    def __str__(self):
        return self.match.__str__

    def set_registry(self, value):
        pass


class MockSymbol(MatchTrackingMixin, Symbol):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.registries = []

    def set_registry(self, value):
        self.registries.append(value)

    def match(self, content: str, index: int) -> Match:
        pass

